# nodejs-communist-problem

This is about evidence of nodejs community members harass and persecute people they disagree with. It is a safe space to talk about the nodejs community slip into the totalitarian ideology called Communism. Evidence is accepted via PRs but opinions are limited to issues. We welcome feedback from all races, genders, non-conforming identities, and even Boomers.

* * *

Hello nodejs community. I have a solemn and sincere request I would like to register with you. I'm not a contributor to node but a user. I just love JS and node!

**Is your feature request related to a problem? Please describe.**

 Nodejs has a Commie problem. 😞

Communism is an ideology which has resulted in the deaths of over a hundred million human beings in the 20th century alone. Communists, also known as Socialists, continue to destroy cultures, races, communities, families, children, and even basic concepts like **gender**. Communists are fundamentally and staunchly anti-family, anti-heterosexual, anti-male, and anti-white.

These people have targeted nodejs for destruction. Radicals are attempting to enforce their  ideology under the guise of false retroactive justice and sweet sounding things like [CoC](https://github.com/nodejs/admin/blame/a7c7d0a8b51109d8f6416a1572e649cc90cba20d/CODE_OF_CONDUCT.md) and [gender pronouns](https://github.com/nodejs/node/blame/b0f75818f39ed4e6bd80eb7c4010c1daf5823ef7/README.md#L153). #21803 #19269

Who could be opposed to that? Right? In reality the best and most reasonable contributors will be alienated and ejected from the nodejs community via these cynical tricks. It has nothing to do with inclusion or respect but rather the opposite! The totalitarian radicals will abuse and enforce their minority view on whoever disagrees with their political agenda.

**Describe the solution you'd like**

1. Contact people who have been unjustly alienated and ejected from the community already to get their view.
2. Give a unified, strong, and complete rejection of Communist ideology and Communist radicals from nodejs.
3. An apology is issued to the nodejs contributors and businesses who were harmed.
4. A full explanation of why the leadership allowed, encouraged, and capitulated to Communists in the first place.

Thank you so much for your time and consideration!
\-Tony Crowe, JS developer

See also [Code Communists](/tcrowe/code-communists) to see how the industry enforces their ideology.

* * *

Some low level nodejs members censored the original posts so the dicussion has moved ➡️ [Proposal: Save nodejs from conformity to Communist ideology](https://gitlab.com/tcrowe/nodejs-communist-problem/issues/1)

<https://github.com/nodejs/node/issues/28503>
<https://github.com/nodejs/TSC/issues/726>

* * *

## Table of Contents

+ [The goal](#the-goal)
+ [Resources about Communism fundamentals](#resources-about-communism-fundamentals)
+ [Rich Trott](#rich-trott)
+ [Myles Borins](#myles-borins)
+ [Bryan Hughes](#bryan-hughes)
+ [addaleax](#addaleax)
+ [Tierney Cyren](#tierney-cyren)
+ [Dan Shaw](#dan-shaw)
+ [Adam Miller](#adam-miller)
+ [Anatoli Papirovski](#anatoli-papirovski)
+ [Issues pages](#issues-pages)
+ [Related individuals](#related-individuals)
+ [Articles](#articles)

## The goal

👀 Will you look with your eyes? Can these people be relied on for unbiased community relations for node?

If you review the information and you find them to be lacking in objectivity please take the necessary steps to spread the word and see they are swiftly removed from node.

## Resources about Communism fundamentals

Before you comment can you get some knowledge about how many people died first?

Democide estimates (people killed mostly by their own Commie government)
<https://www.hawaii.edu/powerkills>

Hitler and the Nazis were National Socialists. Communists are International Socialists.

Soviet Communist mass murder and "gulag" slave labor camps:
<https://en.wikipedia.org/wiki/Excess_mortality_in_the_Soviet_Union_under_Joseph_Stalin>
<https://www.youtube.com/results?search_query=soviet+mass+murder>
<https://en.wikipedia.org/wiki/Mass_killings_under_communist_regimes#Soviet_Union>

Chinese Communist so-called "Great Leap Forward" into mass murder:
<https://www.washingtonpost.com/news/volokh-conspiracy/wp/2016/08/03/giving-historys-greatest-mass-murderer-his-due/>
<https://www.americanthinker.com/blog/2010/09/who_holds_the_title_of_greates.html>
<https://www.youtube.com/results?search_query=mao+great+leap+forward+mass+murder>

Soviet Communist Ukrainian genocide:
<https://en.wikipedia.org/wiki/Holodomor>
<https://www.youtube.com/results?search_query=holodomor>

Cambodian, Pol Pot, Communist "Killing Fields" genocide:
<https://www.youtube.com/results?search_query=communist+killing+fields>

Soviet genocide of the Kazakh peoples:
<https://www.youtube.com/watch?v=JNGHagc5tQ8>
<https://www.youtube.com/results?search_query=kazakh+genocide>

## Rich Trott

<https://github.com/Trott>
<https://twitter.com/trott>

+ LGBT identity politics
+ Black identity politics
+ Countering black identity politics
+ Promoting the abortion industry
+ Promoting big corporate vaccine interests
+ Clinton supporter (a well-known international criminal family)
+ Countering Jewish victimization narrative

![rich-trott-black-identity-politics-retweets01.png](./backups/rich-trott-black-identity-politics-retweets01.png)
![rich-trott-counters-black-narratives01.png](./backups/rich-trott-counters-black-narratives01.png)
![rich-trott-lgbt-politics-retweets01.png](./backups/rich-trott-lgbt-politics-retweets01.png)
![rich-trott-pro-abortion-retweets01.png](./backups/rich-trott-pro-abortion-retweets01.png)
![rich-trott-pro-abortion-retweets02.png](./backups/rich-trott-pro-abortion-retweets02.png)
![rich-trott-pro-abortion-retweets03.png](./backups/rich-trott-pro-abortion-retweets03.png)
![rich-trott-pro-abortion-retweets04.png](./backups/rich-trott-pro-abortion-retweets04.png)
![myles-borins-rich-trott-countering-jewish-victimization-narrative01.png](./backups/myles-borins-rich-trott-countering-jewish-victimization-narrative01.png)

## Myles Borins

<https://github.com/MylesBorins>
<https://twitter.com/MylesBorins>

Political positions:

+ LGBT identity politics
+ Black identity politics
+ Prostitution supporter

Other

+ Questioning Jewish racial composition

![myles-borins-lgbt-politics-retweets01.png](./backups/myles-borins-lgbt-politics-retweets01.png)
![myles-borins-lgbt-politics-retweets02.png](./backups/myles-borins-lgbt-politics-retweets02.png)
![myles-borins-lgbt-politics-retweets03.png](./backups/myles-borins-lgbt-politics-retweets03.png)
![myles-borins-pro-prostitution-retweets01.png](./backups/myles-borins-pro-prostitution-retweets01.png)
![myles-borins-rich-trott-countering-jewish-victimization-narrative01.png](./backups/myles-borins-rich-trott-countering-jewish-victimization-narrative01.png)

## Bryan Hughes

Microsoft dev

<https://twitter.com/nebrius>
<https://github.com/nodejs/node/pull/6973>

Political positions

+ LGBT identity politics
+ Countering Holocaust narrative
+ Questioning Jewish racial composition

![bryan-hughes-discuss-jews-racial-composition01.png](./backups/bryan-hughes-discuss-jews-racial-composition01.png)
![bryan-hughes-myles-borins-discuss-jews-racial-composition01.png](./backups/bryan-hughes-myles-borins-discuss-jews-racial-composition01.png)
[bryan-hughes-myles-borins-discuss-jews-racial-composition02.png](./backups/bryan-hughes-myles-borins-discuss-jews-racial-composition02.png)
![bryan-hughes-lgbt-politics-tweets01.png](./backups/bryan-hughes-lgbt-politics-tweets01.png)
![bryan-hughes-lgbt-politics-tweets02.png](./backups/bryan-hughes-lgbt-politics-tweets02.png)
![bryan-hughes-lgbt-politics-tweets03.png](./backups/bryan-hughes-lgbt-politics-tweets03.png)
![bryan-hughes-lgbt-politics-tweets04.gif](./backups/bryan-hughes-lgbt-politics-tweets04.gif)
![bryan-hughes-pushback-against-holocaust-narrative01.png](./backups/bryan-hughes-pushback-against-holocaust-narrative01.png)
[bryan-hughes-pushback-against-holocaust-narrative02.png](./backups/bryan-hughes-pushback-against-holocaust-narrative02.png)

## addaleax

node's addleax communist censor

<https://twitter.com/addaleax>

Political positions

+ LGBT identity politics
+ Anti-White hate

![addaleax-anti-white-hate01.png](./backups/addaleax-anti-white-hate01.png)
![addaleax-anti-white-hate02.png](./backups/addaleax-anti-white-hate02.png)
![addaleax-anti-white-hate03.png](./backups/addaleax-anti-white-hate03.png)
![addaleax-anti-white-lgbt-politics-tweets01.png](./backups/addaleax-anti-white-lgbt-politics-tweets01.png)
![addaleax-nearform-profile.png](./backups/addaleax-nearform-profile.png)
![addaleax-lgbt-politics-tweets01.png](./backups/addaleax-lgbt-politics-tweets01.png)

## Tierney Cyren

![tierney-cyren-profile03.png](./backups/tierney-cyren-profile03.png)

nodejs social media delegates

<https://bnb.im/>
<https://twitter.com/bitandbang>

Political positions

+ Anti-White hate (self hate?)
+ Anti-democracy
+ Anti-Trump
+ LGBT identity politics
+ Hillary Clinton supporter
+ Feminist politics

![tierney-cyren-profile01.png](./backups/tierney-cyren-profile01.png)
![tierney-cyren-profile02.png](./backups/tierney-cyren-profile02.png)
![tierney-cyren-anti-trump01.png](./backups/tierney-cyren-anti-trump01.png)
![tierney-cyren-anti-trump02.png](./backups/tierney-cyren-anti-trump02.png)
![tierney-cyren-anti-trump03.png](./backups/tierney-cyren-anti-trump03.png)
![tierney-cyren-anti-white-lgbt-politics01.png](./backups/tierney-cyren-anti-white-lgbt-politics01.png)
![tierney-cyren-anti-white01.png](./backups/tierney-cyren-anti-white01.png)
![tierney-cyren-anti-white02.png](./backups/tierney-cyren-anti-white02.png)
![tierney-cyren-feminist-politics01.png](./backups/tierney-cyren-feminist-politics01.png)

Will this person represent the greater node community across social media?

## Dan Shaw

![dan-shaw-profile02.png](./backups/dan-shaw-profile02.png)

Node user feedback

Politics

<https://dshaw.com/>
<https://github.com/dshaw>
<https://twitter.com/dshaw>

+ Anti-Trump
+ Anti-White (self hater?)

![dan-shaw-anti-trump01.png](./backups/dan-shaw-anti-trump01.png)
![dan-shaw-anti-white01.png](./backups/dan-shaw-anti-white01.png)
![dan-shaw-anti-white02.png](./backups/dan-shaw-anti-white02.png)
![dan-shaw-anti-white03.png](./backups/dan-shaw-anti-white03.png)
![dan-shaw-profile01.png](./backups/dan-shaw-profile01.png)

## Adam Miller

nodejs.dev web design

![adam-miller-profile01.png](./backups/adam-miller-profile01.png)

<https://twitter.com/millea9>
<https://github.com/amiller-gh>

Political positions

+ Anti-White
+ Anti-Cop
+ Anti-Trump

He believes he is fighting against something called White Supremacy, like Hitler, from WWII. So, he is tagging all the influential people in his area on Twitter and trying to tattle on people he doesn't like. <https://twitter.com/millea9/status/1143929445994000386> <https://www.c-span.org/video/?461379-1/hearing-federal-response-white-supremacy&start=3570> <https://www.plainviewproject.org/>

![adam-miller-anti-trump01.png](./backups/adam-miller-anti-trump01.png)
![adam-miller-anti-trump02.png](./backups/adam-miller-anti-trump02.png)
![adam-miller-anti-tucker01.png](./backups/adam-miller-anti-tucker01.png)
![adam-miller-anti-white-anti-cops01.png](./backups/adam-miller-anti-white-anti-cops01.png)
![adam-miller-ilhan-omar-supporter01.png](./backups/adam-miller-ilhan-omar-supporter01.png)

## Anatoli Papirovski

node TSC, Postmates

+ <https://github.com/apapirovski>

Political positions

+ Anti-White
+ Black identity politics
+ LGBT identity politics

![anatoli-papirovski-anti-white01.png](./backups/anatoli-papirovski-anti-white01.png)
![anatoli-papirovski-black-identity-politics01.png](./backups/anatoli-papirovski-black-identity-politics01.png)
![anatoli-papirovski-lgbt-politics01.png](./backups/anatoli-papirovski-lgbt-politics01.png)

## Issues pages

Related node issues:
<https://github.com/nodejs/node/issues/21803>
<https://github.com/nodejs/node/pull/19269>

## Related individuals

Outer Party / Communist / LGBT enforcers

+ [@trott](https://github.com/trott), UCSF Center for Knowledge Management, University of California San Francisco
+ [@nebrius](https://github.com/nebrius), Senior Cloud Advocate at Microsoft
+ [@addaleax](https://github.com/addaleax) NearForm
+ [@mcollina](https://github.com/mcollina) NearForm
+ <https://github.com/bnb> Microsoft
+ https://github.com/MylesBorins Google
+ https://github.com/dshaw PayPal
+ <https://twitter.com/millea9> LinkedIn
+ https://github.com/apapirovski Postmates

Will be purged / Probably not a Communist or LGBT

+ [@obensource](https://github.com/obensource) node i18n

Unaffiliated /  Bugmen / Will defer to Communist bullying

+ [@rvagg](https://github.com/rvagg), kicked out of node and let back in?
+ <https://github.com/bamieh> node mentorship
+ <https://github.com/AhmadAwais> node outreach
+ <https://twitter.com/waleedashraf01> node collection
+ https://github.com/BethGriggs IBM, could easily lean toward feminist Communism


## Articles

Rod Vagg's article about node organization political shift:
<https://r.va.gg/2018/09/the-perils-of-private-politics-in-open-source.html>

Archives about nodejs community Communists: <http://archive.is/VEtHu>

Isaac Schlueter anti-white male comments:
<https://dailycaller.com/2017/09/25/tech-ceo-isaac-schlueter-calls-for-fewer-white-men-in-tech/>
<https://archive.is/Y1iPl>

NPM exploits and threatens to fire employees for unionizing who want to improve working conditions
<https://www.theregister.co.uk/2019/06/14/npm_union_busting_claims/>

## Copying, license, and contributing

Copyright (C) Tony Crowe 2020 <https://tonycrowe.com/contact/>

Thank you for using and contributing to make nodejs-communist-problem better.

⚠️ Please run `npm run prd` before submitting a patch.

⚖️ nodejs-communist-problem is free and unlicensed. Anyone can use it, fork, or modify and we the community will try to help whenever possible.
